from setuptools import setup, find_packages

setup(
  name = "hassbrainapi", 
  version = "0.0.5",
  url = "https://gitlab.com/hassbrain/hassbrain_api.git", 
  author = "Christian Meier", 
  author_email = "christian@meier-lossburg.de", 
  license = "MIT",
  packages = find_packages(), 
  install_requires = ['requests']
)
